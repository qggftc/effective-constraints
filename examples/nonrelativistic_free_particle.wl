<< "EffectiveConstraints`"

(* This is an example showing some of the capabilities using the free
   nonrelativistic particle. *)

(* Define the quantum algebra. *)
variables = {q, p, t, pt};
operators = op /@ variables;
algebraPartial = {
  commutator[op[q], op[p]] -> I \[HBar],
  commutator[op[q], op[p]] -> I \[HBar]
};
algebra = completeAlgebra[operators, algebraPartial];

order = 2;

(* Compute the quantum phase space up to order \[HBar] (second order in moments). *)
{effectiveVariables, effectivePoissonStructure} =
  effectivePhaseSpace[operators, algebra, order];

(* Define the constraint. *)
constraint = op[pt] + 1 / (2 m) ncTimes[op[p], op[p]];
effConstraints = effectiveConstraints[constraint, operators, algebra, order];
