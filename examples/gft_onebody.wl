<<"EffectiveConstraints`"


vars = {\[DoubleStruckCapitalN], \[CapitalPi]1, \[CapitalPi]2, \[DoubleStruckCapitalX], \[CapitalLambda], \[CapitalKappa]};
ops = op /@ vars;
algPartial = {
  commutator[op[\[DoubleStruckCapitalX]], op[\[CapitalPi]1]] -> I \[HBar] op[\[DoubleStruckCapitalN]], 
  commutator[op[\[DoubleStruckCapitalX]], op[\[CapitalPi]2]] -> I \[HBar] 2 op[\[CapitalPi]1], 
  commutator[op[\[CapitalLambda]], op[\[CapitalKappa]]] -> I \[HBar] \[Alpha] op[\[CapitalKappa]]
};
alg = completeAlgebra[ops, algPartial];


(* Define the quantum constraint.
 * Cf. Equation (4.1) *)
\[DoubleStruckCapitalC] = m^2 op[\[DoubleStruckCapitalN]] - op[\[CapitalLambda]] - \[Lambda] op[\[CapitalPi]2];
(* Compute a list of effective constraints.
 ( Cf. Equations (4.3) *)
eff\[DoubleStruckCapitalC]s = effectiveConstraints[\[DoubleStruckCapitalC], ops, alg, 2];


(* Compute the truncated quantum phase space. *)
{effVars, effPS} = effectivePhaseSpace[ops, alg, 2];


(* Define gauge fixing conditions.
 * Cf. Equation (4.4) *)
\[DoubleStruckCapitalG]s = Map[
  moment[op[\[CapitalKappa]], #]&,
  Complement[ops, {op[\[CapitalLambda]]}]
];
(* List of rules which impose the gauge fixing conditions. *)
repl\[DoubleStruckCapitalG]s = Map[
  Rule[#, 0]&,
  \[DoubleStruckCapitalG]s
];


(* Variables to eliminate via the effective constraints.
 * (Cf. the discussion below Eq. (4.3).) *)
toEliminate = Join[
  {expVal[op[\[CapitalLambda]]]},
  Map[
    moment[op[\[CapitalLambda]], #]&,
    ops
  ]
];
(* List of rules which implement the effective constraints. *)
repl\[DoubleStruckCapitalC]s = First @ Solve[
  0 == # & /@ eff\[DoubleStruckCapitalC]s,
  toEliminate
];


(* The variables spanning the gauge-fixed constraint hypersurface.
 * Overscript[\[ScriptCapitalV], ~] in the main text. *)
remainingVars = Complement[
  effVars,
  toEliminate,
  \[DoubleStruckCapitalG]s
];


(* Function which imposes both effective constraints and gauge
 * fixing conditions. *)
toGaugeFixedConstraintHypersurface = Composition[
  ReplaceAll[repl\[DoubleStruckCapitalG]s],
  ReplaceAll[repl\[DoubleStruckCapitalC]s]
];


(* The matrix of Poisson brackets of the gauge fixing conditions \[DoubleStruckCapitalG]_a 
 * and the effective constraints \[DoubleStruckCapitalC], \[DoubleStruckCapitalC]_a
 * on the gauge-fixed constraint hypersurface.
 * Cf. Equation (4.5) *)
pbMatrix =
  toGaugeFixedConstraintHypersurface @
  Outer[
    poissonBracket[#1, #2, effVars, effPS]&,
    \[DoubleStruckCapitalG]s,
    eff\[DoubleStruckCapitalC]s
  ];


(* Determine which flow is nontrivial on the gauge-fixed constraint
 * hypersurface.
 * (Cf. the discussion below (4.5). ) *)
unfixedConstraintsIndices = Position[Transpose @ pbMatrix, {0..}];
unfixedConstraints = Extract[eff\[DoubleStruckCapitalC]s, unfixedConstraintsIndices];
flowNonTrivialQ[constraint_] := Not @ MatchQ[{0..}] @ Map[
 toGaugeFixedConstraintHypersurface @
   poissonBracket[constraint, #, effVars, effPS]&,
 remainingVars
];
{hamiltonianConstraint} = Select[unfixedConstraints, flowNonTrivialQ];


(* The set of differential equations on the reduced phase space.
 * The time-dependent function are wrapped in `u` and the initial
 * values are wrapped in `initial`.
 * (Cf. Equations [eq:eom_reduced_phase_space_nontrivial].) *)
toTimeDep = ReplaceAll[Rule[#, u[#][t]]& /@ remainingVars];
funcs = u /@ remainingVars;
eomsLHS = D[toTimeDep @ remainingVars, t];
eomsRHS = toTimeDep @ Map[
  poissonBracket[#, hamiltonianConstraint, effVars, effPS]&,
  remainingVars
];
eoms = Thread[Equal[eomsLHS, eomsRHS]];
eomsInitial = Map[
  u[#][0] == initial[#]&,
  remainingVars
];


(* Solutions to the equations of motion.
 * (Cf. Equations [eq:solutions_reduced_phase_space_nontrivial].) *)
solutions = First @ DSolve[
  Join[eoms, eomsInitial],
  funcs,
  t
];
