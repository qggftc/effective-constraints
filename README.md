# Readme

This repository contains the code of the Mathematica package
`EffectiveConstraints` which may be used to perform calculations in the
effective quantum phase space spanned by expectation values and moments.

The package was developed and tested in Mathematica 12.

## Installation

A simple way to install the package is to place the directory
`EffectiveConstraints` into one of the `Applications` directories in
Mathematica's `$Path`. Suitable locations can be listed via

```wl
Select[StringContainsQ["Applications"]] @ $Path
```

On UNIX-like systems we recommend that the user places the package inside their
home directory

```
~/.Mathematica/Applications
```

## Usage

The package can be loaded using `<<` (`Get`):

```wl
<< "EffectiveConstraints`"
```

A list of available functions can be obtained using

```wl
Names["EffectiveConstraints`*"]
```

All functions come with a `usage` string which can be accessed by calling

```wl
? functionName
```

For instance, `? expVal`.

Example files can be found in [`examples`](examples).
