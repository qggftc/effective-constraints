BeginPackage["EffectiveConstraints`"]

Get["EffectiveConstraints`Usage`"]

Begin["`Private`"]

Clear[effectiveConstraints]

effectiveConstraints[c_, ops_, alg_, order_Integer] :=
  Composition[
    truncate[#, order]&,
    toMoments[#, ops, alg]&,
    Flatten
  ][
    Table[
      computeEffectiveConstraints[c, ops, n],
      {n, 0, order - 1}
    ]
  ]

Clear[computeEffectiveConstraints]

computeEffectiveConstraints[c_, _,  0] := expVal[c]
computeEffectiveConstraints[c_, ops_, n_Integer] :=
  Module[{polyOrderings},
    polyOrderings = DeleteDuplicates[Sort /@ Tuples[ops, {n}]];
    Table[
      expVal[
        ncTimes @@ Join[
          ReleaseHold[
            HoldPattern[# - expVal[#]]& /@ polyOrdering
          ],
          {c}
        ]
      ],
      {polyOrdering, polyOrderings}
    ]
  ]

End[]

EndPackage[]
