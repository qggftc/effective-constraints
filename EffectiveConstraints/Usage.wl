BeginPackage["EffectiveConstraints`"]

expVal::usage =
"expVal[x] computes the expectation value of the operator x.";

moment::usage =
"moment[x1, ..., xn] denotes the nth moment of the xs.";

toMoments::usage =
"toMoments[expr, alg] converts all expectation values of more than one
operator to the corresponding moments.";

toExpVal::usage =
"toExpVal[expr] converts all moments present in expr to their Weyl
ordered expansion in terms of expectation values.";

truncate::usage =
"truncate[expr, ord] truncates expr to order ord in the moments. Effectively an
expansion to order ord/2 in \[HBar].";

effectivePhaseSpace::usage =
"effectivePhaseSpace[ops, alg, ord] computes the effective phase space of ops up
to order ord.

Output is of the form {effectiveVariables, effectivePoissonStructure} where
  * effectiveVariables is a list of effectives variables
  * effectivePoissonStructure is a matrix of Poisson brackets of the effective
    variables";

effectiveConstraints::usage =
"effectiveConstraints[c, ops, alg, ord] gives the effective constraints of the
quantum constraint c up to order \[HBar]^(ord / 2).

If the algebra is not fully specified, it is assumed that commutators are of the
order \[HBar].";

poissonBracket::usage =
"poissonBracket[x, y, vars, ps] computes the Poisson bracket of x and y.
    * vars: List of variables
        Example: {q, p}
    * ps: Matrix corresponding to the Poisson structure of 'vars'
        Example: {{0, 1}, {-1, 0}}";

diracBracket::usage =
"diracBracket[x, y, vars, ps, cs, delta] computes the Dirac bracket of x and y,
where cs are constraints and delta is the inverse of the Poisson bracket matrix
of the constraints.";

op::usage =
"op[x] represents the operator x.";

ncTimes::usage =
"ncTimes[x, y, ...] represents the noncommutative product of
operators. Only expressions wrapped in 'op' are considered to be operators.";

ncPower::usage =
"ncPower[x, n] gives the noncommutative product of x n times with itself.";

commutator::usage =
"commutator[x, y] gives the commutator of x and y.";

completeAlgebra::usage =
"completeAlgebra[ops, alg] gives the completion of a partial algebra by
antisymmetrizing and filling in the missing commutators with trivial
commutators.";

ordering::usage =
"ordering[expr] brings the arguments of ncTimes into lexical order.
ordering[expr, order] brings the arguments of ncTimes into the same same order
as supplied by the optional argument order = {op[1], op[2], op[3], ...}.";

applyAlgebra::usage =
"applyAlgebra[expr, alg] replaces commutators with their respective entry in
alg.";

EndPackage[]
