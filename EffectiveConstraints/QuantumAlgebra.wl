BeginPackage["EffectiveConstraints`"]

Get["EffectiveConstraints`Usage`"]

Begin["`Private`"]

Clear[op]


Clear[ncTimes]

ncTimes[] := 1

ncTimes[x___, ncTimes[y___], z___] := ncTimes[x, y, z]

ncTimes[x___, Plus[y1_, y2__], z___] := ncTimes[x, y1, z] + ncTimes[x, Plus[y2], z]

ncTimes[x_op] := x
ncTimes[x_commutator] := x

ncTimes[x___, c_expVal, y___] := c ncTimes[x, y]
ncTimes[x___, c_expVal y__, z___] := c ncTimes[x, Times[y], z]

ncTimes[x___, c_, y___] /; FreeQ[c, op] := c ncTimes[x, y]
ncTimes[x___, c_ y__, z___] /; FreeQ[c, op] := c ncTimes[x, Times[y], z]



Clear[ncPower]

ncPower[x_, n_Integer] := ncTimes @@ Table[x, n]


Clear[commutator]

commutator[x_, x_] := 0

commutator[x_, _] /; NumericQ[x] := 0
commutator[_, x_] /; NumericQ[x] := 0

commutator[HoldPattern[ncTimes[x_, y__]], z_] :=
  ncTimes[commutator[x, z], y] + ncTimes[x, commutator[ncTimes[y], z]]

commutator[x_, HoldPattern[ncTimes[y_, z__]]] :=
  ncTimes[commutator[x, y], z] + ncTimes[y, commutator[x, ncTimes[z]]]

commutator[c_ x_, y_] /; FreeQ[c, op] := c commutator[x, y]
commutator[x_, c_ y_] /; FreeQ[c, op] := c commutator[x, y]

commutator[x_ + y_, z_] := commutator[x, z] + commutator[y, z]
commutator[x_ , y_ + z_] := commutator[x, y] + commutator[x, z]


Clear[completeAlgebra]

completeAlgebra[ops_List, alg_] /; (
  MatchQ[ops, {__op}]
):=
  Module[{
    antisymmetrized,
    rest
  },
    antisymmetrized = Flatten @ (
      {#, # /. ((commutator[a_, b_] -> c_) :> (commutator[b, a] -> -c))}& /@ alg
    );
    rest = (commutator[#1, #2] -> 0)& @@@ Complement[
      DeleteCases[{x_, x_}] @ Tuples[ops, 2],
      Replace[antisymmetrized, (commutator[a_, b_] -> _) :> {a, b}, 1]
    ];
    Join[antisymmetrized, rest]
  ]


Clear[commutatorReduce]

(* The reason of introducing this helper function is that defining such a rule
   for "commutator" would result in the need to wrap all left-hand sides
   containing a pattern with "commutator" in "HoldPattern". *)
commutatorReduce[expr_] :=
  Module[{localCommutator},
    localCommutator[x_, _] /; (FreeQ[op][x] && FreeQ[commutator][x]) = 0;
    localCommutator[_, x_] /; (FreeQ[op][x] && FreeQ[commutator][x]) = 0;
    expr /. {commutator -> localCommutator} /. {localCommutator -> commutator}
  ]


Clear[applyAlgebra]

applyAlgebra[expr_, alg_] :=
  FixedPoint[commutatorReduce @ ReplaceRepeated[#, alg] &, expr]
applyAlgebra[alg_] := applyAlgebra[#, alg]&


Clear[orderedPairs]

orderedPairs[l_List] :=
  {l[[#1]], l[[#2]]} & @@@ Subsets[#, {2}] &@ Range[Length[l]]


Clear[pairToRule]

pairToRule[{a_, b_}] :=
  ncTimes[PatternSequence[x___, b, a, y___]] :>
    ncTimes[x, a, b, y] - ncTimes[x, commutator[a, b], y]


Clear[extractArgs]

extractArgs[expr_] :=
  DeleteDuplicates @ Sort @ Flatten @ Last @ Reap[
    expr /. HoldPattern[x : ncTimes[args__]] :> (Sow[{args}]; x)
  ]


Clear[orderingAux]

orderingAux[expr_, order_:{}] :=
  Module[{args, rules},
    args = If[
      order === {},
      extractArgs[expr],
      order
    ];
    rules = pairToRule /@ (orderedPairs @ args);
    ReplaceRepeated[expr, rules]
  ]


Clear[ordering]

ordering[expr_] := FixedPoint[orderingAux, expr]
ordering[expr_, order_] := FixedPoint[orderingAux[#, order]&, expr]

End[]

EndPackage[]
