BeginPackage["EffectiveConstraints`"]

Get["EffectiveConstraints`Usage`"]

Begin["Private`"]

poissonBracket[x_, y_, vars_, ps_] :=
  Grad[x, vars] . ps . Grad[y, vars]

diracBracket[x_, y_, vars_, ps_, cs_, delta_] :=
  poissonBracket[x, y, vars, ps] -
  Dot[
    poissonBracket[x, #, vars, ps]& /@ cs,
    delta,
    poissonBracket[#, y, vars, ps]& /@ cs
  ]

End[]

EndPackage[]
