BeginPackage["EffectiveConstraints`"]

Get["EffectiveConstraints`Usage`"]

Begin["`Private`"]

\[HBar] = Global`\[HBar];

Clear[expVal]

SetAttributes[expVal, Listable]

expVal[c_?NumericQ] := c
expVal[c_] /; FreeQ[c, op] := c
expVal[c_ x___] /; FreeQ[c, op] := c expVal[Times[x]]
expVal[Plus[y1_, y2__]] := expVal[y1] + expVal[Plus[y2]]
expVal[Power[c_expVal, n_.] x_.] := c^n expVal[Times[x]]


Clear[moment]

SetAttributes[moment, Orderless]

moment[] := 1

moment[x___, c_, z___] /; (FreeQ[op][c] && FreeQ[commutator][c]) := c moment[x, z]
moment[x___, c_ y__, z___] /; (FreeQ[op][c] && FreeQ[commutator][c]) := c moment[x, Times[y], z]

moment[x___, y1_ + y2__, z___] := moment[x, y1, z] + moment[x, Plus[y2], z]


Clear[weylOrdering]

weylOrdering :=
  ReplaceAll[
    HoldPattern[expVal[ncTimes[xs__]]]:> weylExpVal[ncTimes[xs]]
  ]


Clear[weylExpVal]

weylExpVal[HoldPattern[ncTimes[x__]]] :=
  Module[
    {
      permutations
    },
    permutations = Permutations[{x}];
    1/Length[permutations] * expVal[Plus @@ ncTimes @@@ Flatten /@ permutations]
  ]


Clear[toMoments]

toMoments[expr_] := toMoments[expr, {}]
toMoments[expr_, ops_, alg_] := toMoments[expr, alg]
toMoments[expr_, alg_] :=
  Module[{repl},
    repl = HoldPattern[x : expVal[ncTimes[xs__]]] :> (
      x
      + moment[xs]
      - momentExpand[xs]
      (* The above adds and subtracts the same expression, but only expands the
         moment in terms of the expectation values for the subtracted part. *)
    );
    FixedPoint[
      Composition[
        Simplify,
        ordering,
        ReplaceAll[repl]
      ],
      ordering[expr]
    ] // applyAlgebra[#, alg]&
  ]


Clear[toExpVal]

toExpVal[expr_] :=
  ReplaceAll[HoldPattern[moment[xs__]] :> momentExpand[xs]] @ expr


Clear[momentExpand]

momentExpand[xs__] :=
  weylOrdering @ expVal[ncTimes @@ Table[x - expVal[x], {x, {xs}}]]


Clear[effPoissonBracket]

(* Basic properties of the Poisson bracket *)

effPoissonBracket[x_?NumericQ, y_, ___] := 0
effPoissonBracket[x_, y_?NumericQ, ___] := 0
effPoissonBracket[x_, x_, ___] := 0

effPoissonBracket[Plus[x_, y__], z_, ops_, alg_] :=
  effPoissonBracket[x, z, ops, alg] + effPoissonBracket[Plus[y], z, ops, alg]
effPoissonBracket[x_, Plus[y_, z__], ops_, alg_] :=
  effPoissonBracket[x, y, ops, alg] + effPoissonBracket[x, Plus[z], ops, alg]

effPoissonBracket[Power[x_, n_Integer], y_, ops_, alg_] :=
  n Power[x, n-1] effPoissonBracket[x, y, ops, alg]
effPoissonBracket[x_, Power[y_, n_Integer], ops_, alg_] :=
  n Power[y, n-1] effPoissonBracket[x, y, ops, alg]

effPoissonBracket[Times[x_, y__], z_, ops_, alg_] :=
  x effPoissonBracket[Times[y], z, ops, alg] + Times[y] effPoissonBracket[x, z, ops, alg]
effPoissonBracket[x_ , Times[y_, z__], ops_, alg_] :=
  y effPoissonBracket[x, Times[z], ops, alg] + Times[z] effPoissonBracket[x, y, ops, alg]

HoldPattern[effPoissonBracket[expVal[x_], expVal[y_], ops_, alg_]] :=
  Composition[
    toMoments[#, alg]&,
    applyAlgebra[#, alg]&
  ][
    1 / (I \[HBar]) * expVal[commutator[x, y]]
  ]

effPoissonBracket[x_moment, y_, ops_, alg_] :=
  Composition[
    Simplify,
    toMoments[#, alg]&
  ][
    effPoissonBracket[toExpVal[x], y, ops, alg]
  ]

effPoissonBracket[x_, y_moment, ops_, alg_] :=
  - effPoissonBracket[y, x, ops,alg]


Clear[opsToVars]

opsToVars[ops_List] /; MatchQ[ops, {__op}] :=
  ops /. op -> Identity


Clear[unsortedTuples]

unsortedTuples[l_List, n_Integer] :=
  DeleteDuplicates[Sort /@ Tuples[l, n]]


Clear[truncate]
truncate[order_Integer] := truncate[#, order] &
truncate[expr_, order_Integer] :=
  Module[
    {repl, \[Lambda]},
    repl = ReplaceAll[{
       x:\[HBar] -> \[Lambda]^2 * x,
       moment -> (\[Lambda]^Length[{##}] * moment[##] &),
       commutator -> (\[Lambda]^2 * commutator[##] &)
       }];
    Normal@Series[repl@expr, {\[Lambda], 0, order}] /. \[Lambda] -> 1
  ]


Clear[effectiveVariables]

effectiveVariables[ops_List, ord_Integer] :=
  Join[
    expVal /@ ops,
    moment @@@ Join @@ (
      Table[unsortedTuples[ops, n], {n, 2, ord}]
    )
  ]


Clear[effectivePoissonStructure]

effectivePoissonStructure[effVars_, ops_List, alg_List, ord_Integer] :=
  Module[{n},
    n = Length[effVars];
    truncate[ord] @ Simplify @ Normal @ SparseArray[
      Flatten[#, 2]& @ Table[
        With[{val = effPoissonBracket[effVars[[$i]], effVars[[$j]], ops, alg]},
          {{$i, $j} -> val, {$j, $i} -> - val}
        ],
        {$i, 1, n-1}, {$j, $i, n}
      ],
      {n, n}
    ]
  ]


Clear[effectivePhaseSpace]

effectivePhaseSpace[ops_List, alg_List, ord_Integer] :=
  Module[{effVars, effPS},
    effVars = effectiveVariables[ops, ord];
    effPS = effectivePoissonStructure[effVars, ops, alg, ord];
    {effVars, effPS}
  ]

End[]

EndPackage[]
